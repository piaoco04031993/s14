// Writing Comments
	// Comments are part of  the code that gets ignored byt the language
	// Comments are meant to describe the written code 

	// Two Types of Comments:
		/*For Multi-lines= ctrl + shft + /
			Denoted by slash and asterisk
		*/
		// For single line= ctrl + / 
			// Denoted by two forward slashes

// Checking if index.html is connected to index.js
	// alert('hi');
	// Syntax: alert('message')
	// 		   alert ("message")
	//         alert (^message^)

// Storing Values 
	// Variables
		// it is used to contain data.
		// when we create variables, a certain portion of a devices memory is given a 'name' that we call 'variables'
		// this makes it easier for us to associate information stored in our devicesto actual 'names'about information.

// Declaring Variables
	// Syntax: let/const variablename;
	//			let/conts = value;
let myVariable;

// to print variables, console.log() is useful
// constant use of this throughout developing an application will save us time and builds good habit in always checking for the output of our code
console.log(myVariable);
	// result- undefined

// console.log(hello);
// 	// result- hello is not defined


// let hello;
// 	// result- cannot acces 'hello' before initializing
// 	// Variables must be declared before they are used

let productName = 'desktop computer'
console.log(productName) ;

let productPrice= 18999; 
console.log(productPrice);

// this is a mathematical or scientific truth that will not change overtime
const PI = 3.1416;
console.log(PI);
const gravity = 9.31;
console.log(gravity)


// Re-assigning Variables
	// let Variable
productName = 'Laptop'
console.log(productName)
	// result - laptop


	// constant variable
// gravity = 9.32;
	//result- error: assignmnt to constant variable


	
	// Multiple variable declarations
	//  declaring multiple variables in one line
let anime = 'KNY', season = 'Mugen Train Arc';
console.log(anime);

//  Though it is quicker tp declare multiple variables, it is still best practice to use multiple let/ const keyword when declaring variables.
	// Reason- easier to read the code and determine what kind of variable has been created.


let anime1 = 'KNY';
let season1 = 'Mugen Train Arc';
console.log(anime1, season1)


// // Using a reserved keyword as a variable
// const let = hello;
// console.log(let);
// 	//  Result- error: let is disaallowed as lexically bound name


// Strings
	// Series of Characters that create a word, phrase, sentence or anything related to creating text
	// this should wrapped around a double qoutes (" ") or a single qoutes (' ') or back tics (` `)
let company = 'Marvel'
let movie = 'The Eternals'

// Concatenating Strings
		// Multiple string values can be combined to create a single string using the '+' symbol
let nowShowing = company + ' ' + movie
console.log(nowShowing);

let hobby ='I am going to watch' + ' ' + movie;
console.log(hobby);

// Creating new line
// \n creates a new line
// \ is an escape character in string
let mailAddress ='Metro Manila\n\n\Philippines';
console.log(mailAddress);

// John's bag
let owner = 'John\'s bag';
console.log(owner);
let message = "Eren's friends betrayed him";
console.log(message);


// Numbers
// Whole Numer
let numberOfPeople = 26
console.log(numberOfPeople)

// Decimal Numbers
let grade = 95.8;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance)

// Combining text and number
console.log("Armin's grade last semester is" + " " + grade)

// Boolean
	// normally used to store values relating to the state of a certain things
let isMarried = false;
let isSingle = true;
console.log('isMarried' + ' ' + isMarried);
console.log('isSingle' + ' ' + isSingle)

// Arrays
// Array start with index 0
// Syntax: let / const arrayName = [elementA, elementB, elementC, ...]

let fruits = ["dragonfruit", "mangosteen", "apples", "avocado"];
console.log(fruits)


// Different data types in an array
// Although you can store different data types in an array, this is not recommended.
let random = ["Mikasa", 23, true];
console.log(random);


// Object
// used to create complex data that contains pieces of information relevant to each other.
// Syntax:
	// let / const objectName = {
		//  propertyA: value,
		//  propertyB: value
		// }

let character = {
	fullName: 'Kamado Tanjiro',
	age: 15,
	isMarried: false,
	contactNumber: ['+63900000000', '8123 4556'],
	address: {
		houseNumber: '256',
		city: 'Tokyo'
	}
};

console.log(character)

// Null vs. Undefined

// Null
// intentionally express the absence of a value in a variable declaration
// null means that a data type was aasigned to a variable but does not hold any value or amount.
let spouse = null;
console.log(spouse);


//  number 0 and empty string is different from null value 
let number = 0;
let emptyString = '';
console.log(number);
console.log(emptyString);

// undefined
//  represents the state of a variable that has been declared but without an assigned value
let tools;
console.log(tools);

// Functions

// Declaration
// Syntax:
// function functionName(){
	// block of code}

function printName(){
	console.log('My name is Tony')
};

// invoking function/ calling functiion
// suntax: functionName();
printName();

function movielist(movie) {
	console.log('I will watch' + ' ' + movie)
};

movielist('Shang-chi and the Ten Rings');
movielist('Spiderman: No Way Home');
// movielist('anime');

// using multiple parameters
function createFullName(firstname, middleName, lastName)
{
	alert('welcome' + ' ' + firstname + ' ' + middleName + ' ' + lastName);
}
createFullName('Pia', 'Oco', 'Ogawa');

// using variables as an argument
let firstname = 'Pia'
let middleName = 'Oco'
let lastName = 'Ogawa' 

createFullName(firstname, middleName, lastName)

// Return statement

function returnFullName(firstname, middleName, lastName)
	{
		return firstname + ' ' + middleName + ' ' + lastName
		console.log('This message will not be printed')
	}

let completeName = returnFullName(firstname, middleName, lastName)
console.log(completeName);